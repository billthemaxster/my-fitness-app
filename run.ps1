Invoke-Expression "dotnet build"

if ($Global:LASTEXITCODE -eq 0) {
    Write-Host "Build succeeded, running project"
    Set-Location "./db"
    Invoke-Expression "docker-compose up -d"

    if ($Global:LASTEXITCODE -eq 0) {
        Set-Location ".."
        Invoke-Expression "dotnet run --project ./my-fitness-app/my-fitness-app.csproj"
    }
    else {
        Set-Location ".."
    }
}
