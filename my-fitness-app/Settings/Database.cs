﻿namespace MyFitnessApp.Settings
{
    public record Database
    {
        public const string Key = "Database";
        public string AccessKey { get; init; }
        public string SecretKey { get; init; }
        public string AwsRegion { get; init; }
        public string ServiceUrl { get; init; }
    }
}
