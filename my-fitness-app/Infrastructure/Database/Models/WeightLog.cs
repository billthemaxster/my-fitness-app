﻿using System;
using Amazon.DynamoDBv2.DataModel;

namespace MyFitnessApp.Infrastructure.Database.Models
{
    [DynamoDBTable(TableName)]
    public record WeightLog
    {
        public const string TableName = "weight-log";

        [DynamoDBHashKey]
        public Guid UserId { get; init; }

        [DynamoDBRangeKey]
        public Guid Id { get; init; }

        public decimal WeightInKg { get; init; }

        public DateTime LogDate { get; init; }
        public bool IsDeleted { get; init; }
    }
}
