﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using MyFitnessApp.Infrastructure.Database.Models;
using Microsoft.Extensions.Logging;

namespace MyFitnessApp.Infrastructure.Database
{
    public class DynamoMigrations
    {
        private static class DynamoDbColumnType
        {
            public const string String = "S";
            public const string Numeric = "N";
            public const string Binary = "B";
        }

        private static class DynamoDbKeyType
        {
            public const string Partition = "HASH";
            public const string Sort = "RANGE";
        }

        private readonly IAmazonDynamoDB _dynamoClient;
        private readonly ILogger<DynamoMigrations> _logger;

        public DynamoMigrations(
            IAmazonDynamoDB dynamoDBClient,
            ILogger<DynamoMigrations> logger)
        {
            _dynamoClient = dynamoDBClient;
            _logger = logger;
        }

        public async Task RunAll()
        {
            await InitialiseDatabase();
        }

        private async Task<bool> InitialiseDatabase()
        {
            using (_logger.BeginScope(new { Migration = "InitialiseDatabase" }))
            {
                var request = new ListTablesRequest
                {
                    Limit = 10
                };

                _logger.LogInformation("Getting list of tables");
                var response = await _dynamoClient.ListTablesAsync(request);

                var results = response.TableNames;

                _logger.LogInformation($"Tables: {string.Join(',', results)}");
                if (!results.Contains(WeightLog.TableName))
                {
                    _logger.LogInformation($"Creating table {WeightLog.TableName}");
                    var createRequest = new CreateTableRequest
                    {
                        TableName = WeightLog.TableName,
                        AttributeDefinitions = new List<AttributeDefinition>
                    {
                        new()
                        {
                            AttributeName = nameof(WeightLog.Id),
                            AttributeType = DynamoDbColumnType.String
                        },
                        new()
                        {
                            AttributeName = nameof(WeightLog.UserId),
                            AttributeType = DynamoDbColumnType.String
                        }
                    },
                        KeySchema = new List<KeySchemaElement>
                    {
                        new()
                        {
                            AttributeName = nameof(WeightLog.UserId),
                            KeyType = DynamoDbKeyType.Partition
                        },
                        new()
                        {
                            AttributeName = nameof(WeightLog.Id),
                            KeyType = DynamoDbKeyType.Sort
                        }
                    },
                        ProvisionedThroughput = new(1, 1)
                    };

                    var createTableResponse = await _dynamoClient.CreateTableAsync(createRequest);
                    _logger.LogInformation($"Table {WeightLog.TableName} created successfully");

                    await _dynamoClient.UpdateTableAsync(new UpdateTableRequest
                    {
                        TableName = WeightLog.TableName,
                        BillingMode = BillingMode.PAY_PER_REQUEST
                    });
                    _logger.LogInformation($"Set table {WeightLog.TableName} to on-demand");
                }
                else
                {
                    _logger.LogInformation($"No need to create table {WeightLog.TableName}, already exists");
                }

                return true;
            }
        }
    }
}
