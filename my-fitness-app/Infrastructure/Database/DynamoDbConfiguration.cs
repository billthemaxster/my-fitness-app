﻿using Amazon;
using Amazon.DynamoDBv2;
using Amazon.Runtime;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;

namespace MyFitnessApp.Infrastructure.Database
{
    public static class DynamoDbConfiguration
    {
        public static IServiceCollection AddDynamoDb(this IServiceCollection services, IWebHostEnvironment currentEnv)
        {
            services.AddSingleton<IAmazonDynamoDB>(s =>
            {
                var dbSettings = s.GetService<IOptionsMonitor<Settings.Database>>()
                    .Get(Settings.Database.Key);

                AmazonDynamoDBConfig dynamoConfig = new()
                {
                    RegionEndpoint = RegionEndpoint.GetBySystemName(dbSettings.AwsRegion)
                };

                if (currentEnv.IsDevelopment())
                {
                    dynamoConfig.ServiceURL = dbSettings.ServiceUrl;
                }

                BasicAWSCredentials awsCredentials = new(dbSettings.AccessKey, dbSettings.SecretKey);

                return new AmazonDynamoDBClient(awsCredentials, dynamoConfig);
            });

            services.AddSingleton<DynamoMigrations>();

            return services;
        }
    }
}
