﻿using Microsoft.Extensions.DependencyInjection;

namespace MyFitnessApp.Infrastructure.StaticWrappers
{
    public static class StaticWrapperConfiguration
    {
        public static IServiceCollection AddStaticWrappers(this IServiceCollection services)
        {
            services.AddTransient<IGuidGenerator, GuidGenerator>();

            return services;
        }
    }
}
