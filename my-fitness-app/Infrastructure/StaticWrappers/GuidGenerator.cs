﻿using System;

namespace MyFitnessApp.Infrastructure.StaticWrappers
{
    public interface IGuidGenerator
    {
        Guid NewGuid();
    }

    public class GuidGenerator : IGuidGenerator
    {
        public Guid NewGuid()
        {
            return Guid.NewGuid();
        }
    }
}
