using MyFitnessApp;
using MyFitnessApp.Infrastructure.Database;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

var host = Host.CreateDefaultBuilder(args)
    .ConfigureWebHostDefaults(webBuilder =>
    {
        webBuilder.UseStartup<Startup>();
    })
    .Build();

using (var scope = host.Services.CreateScope())
{
    await scope.ServiceProvider
        .GetRequiredService<DynamoMigrations>()
        .RunAll();
}

await host.RunAsync();
