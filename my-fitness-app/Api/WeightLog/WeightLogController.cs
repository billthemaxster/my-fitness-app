﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace MyFitnessApp.Api.WeightLog
{
    [ApiController]
    [Route("api/[controller]")]
    public class WeightLogController : ControllerBase
    {
        private readonly IMediator _mediator;

        public WeightLogController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IEnumerable<GetAll.Result>> GetAll([FromHeader] Guid userId) =>
            await _mediator.Send(new GetAll.Query(userId));

        public record PostBody(DateTime LogDate, decimal WeightInKg) { }

        [HttpPost]
        public async Task<IActionResult> Create([FromHeader] Guid userId, [FromBody] PostBody body)
        {
            Guid id = await _mediator.Send(new Create.Command
            {
                UserId = userId,
                WeightInKg = body.WeightInKg,
                LogDate = body.LogDate
            });

            return Created($"/weightlog/{id}", id);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromHeader] Guid userId, [FromRoute] Guid id)
        {
            Delete.Result result = await _mediator.Send(new Delete.Command(userId, id));

            return result switch
            {
                WeightLog.Delete.Result.NoMatch => NotFound(),
                WeightLog.Delete.Result.AlreadyDeleted => Conflict(),
                WeightLog.Delete.Result.Success => NoContent(),
                _ => StatusCode((int)HttpStatusCode.InternalServerError)
            };
        }
    }
}
