﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using MediatR;

using Models = MyFitnessApp.Infrastructure.Database.Models;

namespace MyFitnessApp.Api.WeightLog
{
    public static class Delete
    {
        public record Command(Guid UserId, Guid Id) : IRequest<Result> { }

        public enum Result
        {
            Success,
            AlreadyDeleted,
            NoMatch
        }

        public class Handler : IRequestHandler<Command, Result>
        {
            private readonly DynamoDBContext _dynamoClient;

            public Handler(IAmazonDynamoDB dynamoClient)
            {
                _dynamoClient = new(dynamoClient);
            }

            public async Task<Result> Handle(Command request, CancellationToken cancellationToken)
            {
                var search = _dynamoClient.QueryAsync<Models.WeightLog>(request.UserId, new()
                {
                    QueryFilter = new()
                    {
                        new ScanCondition(nameof(Models.WeightLog.Id), ScanOperator.Equal, request.Id)
                    }
                });

                Models.WeightLog entity = (await search.GetRemainingAsync(cancellationToken))
                    .SingleOrDefault();

                if (entity == null)
                {
                    return Result.NoMatch;
                }
                else if (entity.IsDeleted)
                {
                    return Result.AlreadyDeleted;
                }

                await _dynamoClient.SaveAsync(entity with { IsDeleted = true }, cancellationToken);

                return Result.Success;
            }
        }
    }
}
