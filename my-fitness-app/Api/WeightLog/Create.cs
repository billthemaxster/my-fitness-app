﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using MyFitnessApp.Infrastructure.StaticWrappers;
using MediatR;

using Models = MyFitnessApp.Infrastructure.Database.Models;

namespace MyFitnessApp.Api.WeightLog
{
    public static class Create
    {
        public record Command : IRequest<Guid>
        {
            public Guid UserId { get; init; }
            public decimal WeightInKg { get; init; }
            public DateTime LogDate { get; init; }
        }

        public class Handler : IRequestHandler<Command, Guid>
        {
            private readonly DynamoDBContext _dynamoClient;
            private readonly IGuidGenerator _guidGenerator;

            public Handler(
                IAmazonDynamoDB dynamoClient,
                IGuidGenerator guidGenerator)
            {
                _dynamoClient = new(dynamoClient);
                _guidGenerator = guidGenerator;
            }

            public async Task<Guid> Handle(Command request, CancellationToken cancellationToken)
            {
                Models.WeightLog entry = new()
                {
                    UserId = request.UserId,
                    Id = _guidGenerator.NewGuid(),
                    WeightInKg = request.WeightInKg,
                    LogDate = request.LogDate
                };

                await _dynamoClient.SaveAsync(entry, cancellationToken);

                return entry.Id;
            }
        }
    }
}
