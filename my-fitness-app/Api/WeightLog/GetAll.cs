﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using MediatR;

using Models = MyFitnessApp.Infrastructure.Database.Models;

namespace MyFitnessApp.Api.WeightLog
{
    public static class GetAll
    {
        public record Query(Guid UserId) : IRequest<IEnumerable<Result>> { }

        public record Result
        {
            public Guid Id { get; init; }
            public decimal WeightInKg { get; init; }
            public DateTime LogDate { get; init; }
            public decimal WeightInLb => WeightInKg * 2.2m;
            public decimal WeightinSt => WeightInLb / 16m;
        }

        public class Handler : IRequestHandler<Query, IEnumerable<Result>>
        {
            private readonly DynamoDBContext _dynamoClient;

            public Handler(IAmazonDynamoDB dynamoClient)
            {
                _dynamoClient = new(dynamoClient);
            }

            public async Task<IEnumerable<Result>> Handle(Query request, CancellationToken cancellationToken)
            {
                var search = _dynamoClient.QueryAsync<Models.WeightLog>(request.UserId, new()
                {
                    QueryFilter = new()
                    {
                        new ScanCondition(nameof(Models.WeightLog.IsDeleted), ScanOperator.Equal, false)
                    }
                });
                var dbResults = await search.GetRemainingAsync(cancellationToken);

                return dbResults.Select(x => new Result
                {
                    Id = x.Id,
                    WeightInKg = x.WeightInKg,
                    LogDate = x.LogDate
                });
            }
        }
    }
}
